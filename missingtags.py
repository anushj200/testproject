import json
import yaml
import boto3
import sys
import csv

without_tag = {}
getprofilelist = ['default', 'westmgmtaccount', 'westdevaccount','westprodaccount']
#getprofilelist = ['default']

with open(r'tag.yaml') as file:
    check_data = yaml.full_load(file)

filter_names = check_data['check_tags'].keys() 
filter_tags = check_data['check_tags'].values()

for getprofile in getprofilelist:
    getprofilesession = boto3.session.Session(profile_name=getprofile)
    ec2 = getprofilesession.resource('ec2')
    for instance in ec2.instances.all():
        instance_id = instance.instance_id
        instance_tags = instance.tags
        dict = {}
        if instance_tags:
            for item_tag in instance_tags:
                dict.update({item_tag['Key']:item_tag['Value']})
            for item_filter_tag in filter_tags:
                if item_filter_tag not in dict.keys():
                    if getprofile not in without_tag.keys():
                        without_tag.update({getprofile:{instance_id:dict}})
                        break
                    else:
                        without_tag[getprofile].update({instance_id:dict})
                        break
                elif dict[item_filter_tag] == '':
                    if getprofile not in without_tag.keys():
                        without_tag.update({getprofile:{instance_id:dict}})
                        break
                    else:
                        without_tag[getprofile].update({instance_id:dict})
                        break
        else:
            if getprofile not in without_tag.keys():
                without_tag.update({getprofile:{instance_id:dict}})
                break
            else:
                without_tag[getprofile].update({instance_id:dict})
                break


csv_header = ["Instance-ID", "InstanceType", "IP", "Account", "InstanceState"]
csv_header.extend(filter_names)

with open('AWS_Servers_Missing_Tags.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(csv_header)
    for profile in without_tag.keys():
        getprofilesession = boto3.session.Session(profile_name=profile)
        ec2 = getprofilesession.resource('ec2')
        for item_instance in set(without_tag[profile]):
            list = []
            getinstance = ec2.Instance(item_instance)
            if profile == 'default':
                account = 'eastaccount'
            else:
                account = profile
            list.extend([getinstance.instance_id, getinstance.instance_type,getinstance.private_ip_address,account,getinstance.state['Name']])
            for item_tag in filter_tags:
                if item_tag not in without_tag[profile][item_instance].keys():
                    list.append('NONE')
                elif without_tag[profile][item_instance][item_tag] == '':
                    list.append('NONE')
                else:
                    list.append(without_tag[profile][item_instance][item_tag])
            writer.writerow(list)
